
# 基于nodejs expressjs，mongodb的简单cms
**预览地址**
[http://101.200.130.104:3000/](http://101.200.130.104:3000/)
> 目前已支持后台文章curd，用户curd，后台用户登录，简单翻页，图片、文件上传，富文本编辑器等。

### Install
> 首先请保证mongodb已经启动
> 数据库配置在common/config.js
```
git clone https://git.oschina.net/man0sions/nodejs-cms.git && cd nodejs-cms
npm install
npm start
```

```
使用测试数据
./mongorestore -d db  path/db/

测试数据在 /common/db
```


```
前台地址：http://localhost:3000/
后台地址：http://localhost:3000/admin
默认管理员：email:admin@admin.com  passwd:admin
```

### 预览
![image](http://101.200.130.104/images/YH/nodecms5.jpg)
![image](http://101.200.130.104/images/YH/nodecms1.jpg)
![image](http://101.200.130.104/images/YH/nodecms2.jpg)
![image](http://101.200.130.104/images/YH/nodecms3.jpg)
![image](http://101.200.130.104/images/YH/nodecms4.jpg)